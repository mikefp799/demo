# Curso Git

*Incluir bagdes del estado del proyecto.*
```shell
December 2019
Author : Mike Francisco (rhodfra@gmail.com)
technologies and programming languages
```
Descripción del proyecto y sus objetivos.


## Screenshots/Project Structure

*Nada dice más que unas capturas de pantalla.* 

## Installation 

La guía de instalación se puede encontrar en el archivo [INSTALL](INSTALL). 

## Usage 

*Explicar como usar el proyecto en caso de ser necesario.* 

## Contributing 

La forma de contribuir se explica en [CONTRIBUTING](CONTRIBUTING).

## Licence 

Los términos generales del licenciamiento del proyecto se encuentran en [LICENCE](LICENSE) en caso de 
requerir mayor información, contactar con el autor.

## Credits

Por ahora, ninguno.

Mike es menso
